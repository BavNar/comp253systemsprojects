﻿using SPolyGames.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SPolyGames.Controllers
{
    public class StudentLoginController : Controller
    {
        // GET: StudentLogin
        public ActionResult StudentLogin()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Authorize(SPolyGames.Models.Student userModel)
        {
            using (PolygameEntities db = new Models.PolygameEntities())
            {
                var userDetails = db.Students.Where(x => x.First_Name == userModel.First_Name && x.S_Password == userModel.S_Password).FirstOrDefault();
                if (userDetails == null)
                {
                    userModel.LoginErrorMessage = "Wrong Username or Password. ";
                    return View("StudentLogin", userModel);
                }
                else
                {
                    Session["UserID"] = userDetails.Studnet_ID;
                    return RedirectToAction("Login", "Home");
                }
                    
            }
            //return View();
        }
    }
}