﻿using SPolyGames.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SPolyGames.Controllers
{
    public class InstructorLoginController : Controller
    {
        // GET: InstructorLogin
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Authorize1(SPolyGames.Models.Instructor userModel)
        {
            using (PolygameEntities2 db = new Models.PolygameEntities2())
            {
                var userDetails1 = db.Instructors.Where(y => y.InsFirst_Name == userModel.InsFirst_Name && y.Ins_Password == userModel.Ins_Password).FirstOrDefault();
                    {
                    if (userDetails1 == null)
                    {
                        userModel.LoginErrorMessage2 = "Wrong Username or Password. ";
                        return View("InstructorLogin", userModel);
                    }
                }
            }
            return View();
        }
    }
}